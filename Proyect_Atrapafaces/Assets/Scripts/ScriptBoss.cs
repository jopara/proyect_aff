using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBoss : MonoBehaviour
{

    public Oleadas oleadas;
    public int vidaActual;
    public float velocidadEmbestida;
    public bool ataque;
    public float vueltaAtras;
    public float vueltaAtrasActual;
    public float tiempoCooldown;
    public float cooldownActual; 

    // Start is called before the first frame update
    void Start()
    {
        oleadas.conteoEnemigos++;
    }

    // Update is called once per frame
    void Update()
    {
        if (ataque == true && vueltaAtras == vueltaAtrasActual)
        {
            transform.position = new Vector3(0, 0, (transform.position.z - (velocidadEmbestida * Time.deltaTime)));
        }

        else if (vueltaAtrasActual > 0f && ataque == false)
        {
            transform.position = new Vector3(0, 0, (transform.position.z - (velocidadEmbestida * Time.deltaTime)));
            vueltaAtrasActual = vueltaAtrasActual - (1 * Time.deltaTime);
            cooldownActual = cooldownActual - (1 * Time.deltaTime);
        }

        else if (vueltaAtrasActual <= 0f && ataque == false)
        {
            
            cooldownActual = cooldownActual - (1 * Time.deltaTime);
        }

        if (cooldownActual <= 0f && ataque == false)
        {
            Debug.Log("AAAAAAAAAAAA");
            ataque = true;
            cooldownActual = tiempoCooldown;
            vueltaAtrasActual = vueltaAtras;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("BBBBBBBBBBBBBBB");
        cooldownActual = tiempoCooldown;
        ataque = false;
        vueltaAtrasActual = vueltaAtras;
    }

    public void Damage(int damage)
    {
        vidaActual -= damage;

        //barraDeVidaBoss.SetHealth(vidaActual);

        if (vidaActual <= 0)
        {
            Muerte();
        }
    }

    public void Muerte()
    {
        Destroy(gameObject);
    }
}
