using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class personaje : MonoBehaviour
{
    public int vidaActual;
    public bool muerto;
    public GameManager gameManager;
    public BarraDeVidaJugador barraDeVidaJugador;


    public float tiempoUltimoDisparo;
    public int tiempoEntreDisparos;
    public GameObject bala;
    public Transform puntoDisparo;

    private AudioSource audioDisparo;

    // Start is called before the first frame update
    void Start()
    {
        audioDisparo = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Damage(1);
        }
    }

    public void disparo()
    {
        if (Time.time - tiempoUltimoDisparo < tiempoEntreDisparos)
        {
            return;
        }

        tiempoUltimoDisparo = Time.time;
        audioDisparo.Play();
        Instantiate(bala, puntoDisparo.position, Quaternion.identity);
        


    }

    public void Damage(int damage)
    {
        vidaActual -= damage;

        barraDeVidaJugador.SetHealth(vidaActual);

        if (vidaActual <= 0)
        {
            Muerte();
        }
    }

    public void Muerte()
    {

        if (vidaActual <= 0)
        {
            gameManager.muerteJugador = true;
            Destroy(gameObject);
            
        }
    }
}
