using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala : MonoBehaviour
{

    public float fuerzaDisparo;
    public Rigidbody rb;
    public int damage = 1;
    public float tiempoDeVida;
    public enemigo enemigoo;
    

    // Start is called before the first frame update
    void Start()
    {
        
        rb.velocity= transform.forward * fuerzaDisparo;
        
        Invoke("destruirBala", tiempoDeVida);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        
    }
    
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("enemigo"))
        {
            collision.GetComponent<enemigo>().Muerte();
            Destroy(gameObject);
        }

        else if (collision.gameObject.CompareTag("boss"))
        {
            collision.GetComponent<boss>().Damage(damage);
            Destroy(gameObject);
        }
    }



    public void destruirBala()
    {
        Destroy(gameObject);
    }
}
