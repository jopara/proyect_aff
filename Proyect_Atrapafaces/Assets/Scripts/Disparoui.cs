using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Disparoui : MonoBehaviour
{

    public Slider slider;
    public personaje personaje;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        slider.maxValue = personaje.tiempoEntreDisparos;
        slider.value = - (personaje.tiempoUltimoDisparo - Time.time);
    }
}
