using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public bool muerteJugador;

    public GameObject CanvasPausa;
    public GameObject CanvasGui;
    public GameObject CanvasVidaBoss;
    public GameObject CanvasHasGanado;
    public GameObject CanvasHasPerdido;

    public Canvas canvasPausa;
    public Canvas canvasGui;
    public Canvas canvasVidaBoss;
    public Canvas canvasHasGanado;
    public Canvas canvasHasPerdido;

    public bool pausa = false;
    public bool estaMuteado;


    // Start is called before the first frame update
    void Start()
    {
        canvasGui.enabled = true;
        canvasPausa.enabled = true;
        canvasVidaBoss.enabled = true;
        canvasHasGanado.enabled = true;
        canvasHasPerdido.enabled = true;

        CanvasGui.SetActive(true);
        CanvasPausa.SetActive(false);
        CanvasVidaBoss.SetActive(false);
        CanvasHasGanado.SetActive(false);
        CanvasHasPerdido.SetActive(false);

        Time.timeScale = 1f;
        pausa = false;
        muerteJugador = false;
        estaMuteado = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (pausa)
        {
            Time.timeScale = 0f;

        }
        else
        {
            Time.timeScale = 1f;
        }

        if (muerteJugador == true)
        {
            MuerteJugador();
        }
    }

    void MuerteJugador()
    {
        CanvasHasPerdido.SetActive(true);


    }

    public void RestartNivel()
    {
        SceneManager.LoadScene("JuegoPrincipal");
    }

    public void Pausado()
    {

        pausa = true;
        CanvasPausa.SetActive(true);
        CanvasGui.SetActive(false);
    }

    public void ContinuarJuego()
    {
        pausa = false;
        CanvasPausa.SetActive(false);
        CanvasGui.SetActive(true);
    }

    public void RegresarMenuPrincipal()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }

    public void CargarJuego()
    {
        SceneManager.LoadScene("JuegoPrincipal");
    }
}
