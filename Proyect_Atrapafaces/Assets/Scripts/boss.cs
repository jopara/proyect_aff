using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boss : MonoBehaviour
{
    public int vidaActual;
    public BarraDeVidaBoss barraDeVidaBoss;

    private Rigidbody rb;
    private Animator anim;

    public personaje player;
    public GameManager gm;
    public Transform objetivo;
    public Transform HomePos;
    private Vector3 posicionInicial;

    public float velocidad;
    public float radioAtaque;
    public float cooldown;
    public float cooldownMax;
    public Collision collision;
    public float velocidadEmbestida;

    public bool embestidaCargada;
    public bool embistiendo;
    public bool muerte;

    float transformAnteriorX;
    float velocidadX;
    float transformAnteriorY;
    float velocidadY;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        //anim.SetBool("embestida", embistiendo);
       // anim.SetBool("muerte", muerte);

        

        if (embistiendo == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, velocidad * Time.deltaTime);
        }
        else if (embistiendo == true)
        {

        }

        /*velocidadX = transform.position.x - transformAnteriorX;
        transformAnteriorX = transform.position.x;

        velocidadY = transform.position.y - transformAnteriorY;
        transformAnteriorY = transform.position.y;*/
    }

    public void Damage(int damage)
    {
        vidaActual -= damage;

        barraDeVidaBoss.SetHealth(vidaActual);

        if (vidaActual <= 0)
        {
            Muerte();
        }
    }

    public void Muerte()
    {
        


        Destroy(gameObject);
    }

    void Embestida()
    {
        if (embestidaCargada == true && cooldown <= 0 && Vector3.Distance(transform.position, player.transform.position) <= radioAtaque)
        {
            cooldown = cooldownMax;
            embestidaCargada = false;
            embistiendo = true;
            //trigger
            objetivo.position = player.transform.position;
        }

        else if (embistiendo == true && Vector3.Distance(transform.position, objetivo.transform.position) > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, objetivo.position, velocidadEmbestida * Time.deltaTime);
        }

        else if (embistiendo == true && Vector3.Distance(transform.position, objetivo.transform.position) <= 0)
        {
            embistiendo = false;
            cooldown = cooldownMax;

        }

        else if (embestidaCargada == false && cooldown > 0 && embistiendo == false)
        {
            cooldown = cooldown - Time.deltaTime;
        }

        else if (embestidaCargada == false && embistiendo == false && cooldown <= 0)
        {
            embestidaCargada = true;
        }

        Muerte();
    }

    public void Vuelve()
    {
        transform.position = Vector3.MoveTowards(transform.position, HomePos.position, velocidad * Time.captureDeltaTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //Debug.Log();
        }
    }

}
