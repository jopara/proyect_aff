using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    //Este script spawnea enemigos, la idea es tener un empty para cada enemigo

    public Oleadas oleadas;
    public int activacion; //este es el numero de oleada en la que el objeto de spawner se activa.
    public bool spawnDisponible; //se usa para que spawnee un enemigo solo en el primer frame y no se ponga a spawnearlos en todos los frames
    //public enemigo enemigo;
    public GameObject enemigo;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //Comprueba en que numero de oleada esta el script de oleadas, y si es el mismo asignado en el script y el spanw esta disponible, crea el enemigo.
        if (activacion == oleadas.oleadaActual && spawnDisponible == true)
        {
            oleadas.conteoEnemigos = oleadas.conteoEnemigos + 1;
            Debug.Log("Spawn" + oleadas.oleadaActual);
            Instantiate(enemigo, transform.position, Quaternion.identity);
            spawnDisponible = false;
        }
    }
}
