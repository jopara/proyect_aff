using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicFondo : MonoBehaviour
{
    private static MusicFondo musicFondo;


    void Awake()
    {
        if(musicFondo == null)
        {
            musicFondo = this;
            DontDestroyOnLoad(musicFondo);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
