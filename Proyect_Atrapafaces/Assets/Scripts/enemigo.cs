using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigo : MonoBehaviour
{

    //public Transform posicionInicial;
    public int velocidad;

    public bool ataque;

    public float vueltaAtras;
    public float vueltaAtrasActual;

    public personaje personaje;
    public Oleadas oleadas;

    // Start is called before the first frame update
    void Start()
    {
        //oleadas.conteoEnemigos = oleadas.conteoEnemigos + 1;
    }

    // Update is called once per frame
    void Update()
    {

        if (ataque == true && vueltaAtras == vueltaAtrasActual)
        {
            transform.position = new Vector3(0, 1, (transform.position.z - (velocidad * Time.deltaTime)));
        }
        

        else if(ataque == false && vueltaAtrasActual > 0f)
        {
            transform.position = new Vector3((transform.position.x - (velocidad * Time.deltaTime * 3)), 0, 0);
            vueltaAtrasActual = vueltaAtrasActual - (1 * Time.deltaTime);
        }
        
        else if(vueltaAtrasActual <= 0f)
        {
            ataque = true;
            vueltaAtrasActual = vueltaAtras;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        //AAAAAAAAAAAAAAA

        if(ataque == true)
        {
            personaje.vidaActual = personaje.vidaActual - 1;
            ataque = false;
        }

        
    }

    public void Muerte()
    {
        oleadas.conteoEnemigos = oleadas.conteoEnemigos - 1;
        Destroy(gameObject);
    }
}
